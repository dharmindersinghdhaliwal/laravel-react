import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import axios from 'axios';

function ListMedia() {
    const [media, setMedia] = useState([])

    useEffect(()=>{
        fetchMedia() 
    },[])

    const fetchMedia = async () => {
        await axios.get(`http://localhost:8000/api/media`).then(({data})=>{
           // Media(data)
        })
    }
    return (
        <div className="container">
          <div className="row">
            <div className='col-12'>
                <a className='btn btn-primary mb-2 float-end' href={"/media/create"}>
                    Insert Image
                </a>
            </div>
            <div className="col-12">
                <div className="card card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered mb-0 text-center">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    media.length > 0 && (
                                        media.map((row, key)=>(
                                            <tr key={key}>
                                            </tr>
                                        ))
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
      </div>
    );
}

export default ListMedia;

// DOM element
if (document.getElementById('listMedia')) {
    ReactDOM.render(<ListMedia />, document.getElementById('listMedia'));
}